
# operator -
def diff(args):
    return args[0]-sum(args[1:])

# operator *
def prod(args):
    acc = args[0]
    for num in args[1:]:
        acc *= num
    return acc

# operator /
def div(args):
    return args[0]/prod(args[1:])

# operator exp
def exp(args):
    return args[0]**args[1]

# operator string-append
def str_append(args):
    return ''.join(args)

# operator =
def cond_eq(args):
    return args[0] == args[1]

# operator <
def cond_less(args):
    return args[0] < args[1]

# operator <=
def cond_leq(args):
    return args[0] <= args[1]

# operator >
def cond_greater(args):
    return args[0] > args[1]

# operator >=
def cond_geq(args):
    return args[0] >= args[1]

# operator list
def list_maker(args):
    return args

# operator car
def car(args):
    return args[0][0]

# operator cd
def cdr(args):
    return args[0][1:]

# operator cons
def cons(args):
    return [args[0]]+args[1]

# operator null?
def null_chk(args):
    return args[0] == []

# operator string-ref
def string_ref(args):
    return args[0][int(args[1])]

# operator substring
def substring(args):
    return args[0][int(args[1]):int(args[2])]

# operator append
def append(args):
    return args[0] + args[1]

# operator list-ref
def list_ref(args):
    return args[0][int(args[1])]

# operator reverse-list
def reverse(args):
    return args[0][::-1]

# operator string-length
def string_len(args):
    return len(args[0])
