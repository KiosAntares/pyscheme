blank = ('\n', '\t')

def lexer(lemma):
    lexed = []                          # This iteration level's lexer result
    word = ''                           # The word currently being prepared
    genenu = enumerate(lemma)           # The iterator over the chars
    in_string = False                   # State: Is currently in a string literal 
                                        # (ignore spaces and symbols)
    in_comment = False

    for i, c in genenu:                 # For each index and value
        if in_comment:
            if c == '\n':
                in_comment = False
            else:
                continue
        elif c == ' ' and not in_string:  # A space is encountered outside a string
            if word != '':              # If there is a word prepared
                lexed += [word]         # Add it to the lexed list
            word = ''                   # Reset word
        elif c == ';' and not in_comment:
            in_comment = True
        elif c == '"':                  # A string delimitation
            word += '"'                 # Add to word   
            in_string = not in_string   # Change invert string status
        
        elif c in ('(', '['):                  # A code block is opened
            innerlexed = lexer(lemma[i+1:]) # The inner block is lexed
            lexed.append(innerlexed[0])     # the results are appended as a nested list
            for _ in range(innerlexed[1]):  # Eeach character worked by the nested
                next(genenu)                # is skipped
        
        elif c in (')', ']'):                  # A code block is closed
            if word != '':              # If a word is prepared
                lexed += [word]         # It is added
            return (lexed, i+1)         # Returns lexed block and number of lexed chars
        elif c in blank:
            continue
        else:                           # For any other chars
            word += c                   # Are added to prepared word
    
    return (lexed, len(lemma))          #Returns outer lexed block and lexed chars
