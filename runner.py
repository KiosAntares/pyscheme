#!/bin/python

from lexer import lexer
from block import block, parse
import sys

datafile = sys.argv[1]
print("Starting execution of file: %s" % datafile)

with open(datafile, 'r') as file:
    data = file.read()
    lexed = lexer(data)
    print("Evaluating tree ", lexed)
    # (tree, parsed_chars_len)
    for blk in lexed[0]:
        print(parse(blk).eval())
