#!/bin/python

from pyscheme import evaluate
from constants import constants
from operators import custom_operators
from custom_printer import custom_str
import readline
import os

def print_c(args):
    if args[0] == 'list-con':
        print([c for c in constants])
    elif args[0] == 'list-op':
        print([f for f in custom_operators])

repl_commands = {
    'exit': lambda _: exit(),
    'clear': lambda _: os.system('cls' if os.name == 'nt' else 'clear'),
    'print': print_c,
}

while True:
    line = input("repl>")
    if line[0] == '!':
        words = line[1:].split()
        if words[0] not in repl_commands:
            print("Not a repl command")
            continue
        repl_commands[words[0]](words[1:])
    else:
        try:
            print(custom_str(evaluate(line)))
        except:
            print("There was an evaluation error")
