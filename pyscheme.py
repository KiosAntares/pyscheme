from lexer import lexer
from block import block, parse

# Evaluates a statement
# Because lexer returns a (tree, analyzed-lenght) tuple, only the tree is given
# Because lexer adds outer list to tree because of (<statement>), so it is stripped
def evaluate(lemma):
    return parse(lexer(lemma)[0][0]).eval()


# a test function to test recursion
test_fib = '(define (fib n) (if (<= n 2) 1 (+ (fib (- n 1)) (fib (- n 2)))))'