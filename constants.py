import math

constants = {
        'null': [],        
        '#true': True,
        '#false': False,
        'pi.0': math.pi,
        'euler.0': math.e,
        '+inf.0': float('inf'),
        '-inf.0': float('-inf'),
        }

reverse_constants = {
    True: '#true',
    False: '#false',
    math.pi: 'pi.0',
    math.e: 'euler.0',
    float('inf'): '+inf.0',
    float('-inf'): '-inf.0',
}
