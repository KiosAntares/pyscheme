from constants import reverse_constants

def custom_str(literal):
    if literal is None:
        return ""
    elif type(literal) is list and len(literal) > 0:
        return '(list ' + ' '.join([custom_str(e) for e in literal]) + ')'
    elif type(literal) is str and len(literal) > 1:
        return '"' + literal + '"'
    elif type(literal) is str and len(literal) == 1:
        return '#\\'+literal
    elif literal in reverse_constants:
        return str(reverse_constants[literal])
    elif type(literal) in (float, int):
        return str(literal)
    elif type(literal) is bool:
        return "#true" if literal else "#false"
    elif literal == []:
        return "null"
