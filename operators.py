from operator_implementations import *

# A custom operators
class custom_operator:
    # Initializer
    def __init__(self, arguments, evaluable):
        self.arguments = arguments  # Arguments identifiers
        self.evaluable = evaluable  # Evaluable block


# Operators are stored as:
# '<operator inside the scheme enviroment>': callback operator 

custom_operators = {}   # Before execution no custom operators are defined

# Callbacks to internal functions implemented in operator_implementations.py
operators = {
        '+': sum,       # The internal python function sum() has the same behavior
        '-': diff,
        '*': prod,
        '/': div,
        'exp': exp,
        'string-append': str_append,
        '=': cond_eq,
        '<': cond_less,
        '<=': cond_leq,
        '>': cond_greater,
        '>=': cond_geq,
        'list': list_maker,
        'car': car,
        'cdr': cdr,
        'cons': cons,
        'null?': null_chk,
        'string-ref': string_ref,
        'substring': substring,
        'char=?': cond_eq,
        'string=?': cond_eq,            # Python can compare strings using the same op.
        'append': append,
        'list-ref': list_ref,
        'reverse-list': reverse,
        'string-length': string_len,
        }
