import re
from constants import constants
from operators import operators, custom_operator, custom_operators

# A generic block of code spawned by a lexa
class block:

    # Constructor: 
    # Parses lexa (keyword) and lexons (arguments) to create a unit that
    # can be evaluated
    def __init__(self, lexa, lexon=[]): 
        is_string_literal = re.compile(r'(".*")')       # Matches "string"
        is_numeric_literal = re.compile(
            r'^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$') # Matches (+/-)num(.digits)
        is_integer_literal = re.compile('^[-+]?\d+$')
        self.raw = lexa                             # Contains the raw lexa (for debug)
        self.rawlexon = lexon                       # Contains the raw lexons (for debug)
        
        # Function or constant definition
        if lexa == 'define':   
            # Function definition block
            # Tree is in form ['define', [<function name>, <arg1>, <arg2>, ...], [<body>]]
            if type(lexon[0]) is list:
                self.type = 'function-def'
                self.name = lexon[0][0]         # The name is the first element of first lexon
                self.arguments = lexon[0][1:]   # The arguments are the other elements  
                self.evaluable = (              # The code block that can be evaluated
                                parse(lexon[1]) 
                                    if type(lexon[1]) is list   # If the element is a list, 
                                                                            # then it is a code block
                                       else block(lexon[1])     # Else it is a constant or identifier
                            )
            # Constant definition block
            # Tree is in form ['define', <constant name>, <constant value> / [<constant value]> ]
            else:
                self.type = 'constant-def'
                self.name = lexon[0]
                self.value = (parse(lexon[1]) if type(lexon[1]) is list 
                             else block(lexon[1]))
        # Enviroment constant definition block
        # Tree is in form 
        # ['let', [[<identifier> <value>], [<identifier> <value>], ...], [<body>]]
        elif lexa == 'let':
            self.type = 'let-block'
            self.variables = {iden: 
                                parse(value) if type(value) is list else block(value)
                                for (iden, value) in lexon[0]} # each [ident., value]
            self.evaluable = parse(lexon[1]) # parsing of the body
        
        # If block
        elif lexa == 'if':
            self.type = 'if-block'
            self.condition = block(lexon[0][0], lexon[0][1:]) # Assertable condition block
            # Block to evaluate if condition is asserted
            self.iftrue = parse(lexon[1])   if type(lexon[1]) is list else block(lexon[1]) 
            # Block to evaluate if condition is not asserted
            self.iffalse = parse(lexon[2])  if type(lexon[2]) is list else block(lexon[2])
        
        # Cond block
        elif lexa == 'cond':
            self.type = 'cond-block'
            self.conditions = []
            self.evaluables = []
            for lex in lexon:
                self.conditions.append(True if lex[0] == 'else' else parse(lex[0]))
                self.evaluables.append(parse(lex[1]) if type(lex) is list else block(lex))

        #A character literal
        elif lexa[0:2] == '#\\':
            self.type = 'character_literal'
            self.value = lexa[2]

        # The keyword is in the constants dictionary
        elif type(lexa) != list and lexa in constants:
            self.value = constants[lexa] # Obtains value from dictionary
            self.type = 'constant_literal' # Is considered literal because already eval.
        
        # The keyword matches the string expression
        elif type(lexa) != list and is_string_literal.match(lexa):
            self.value = lexa[1:-1] # Quotes are removed
            self.type = 'string_literal'

        # The keyword matches the numerical type expression
        elif is_numeric_literal.match(lexa) and lexa not in operators:
            if is_integer_literal.match(lexa):
                self.value = int(lexa)
            else:
                self.value = float(lexa) # Is converted to floating point
            self.type = 'numeric_literal'
        
        # The keyword is unkown and there are no arguments, so assumes identifier
        elif len(lexon) == 0 and lexa not in custom_operators:
            self.type = 'identifier'
            self.name = lexa    # The identifier itself

        # The keyword is unkown and has arguments, so assumes operator
        else:
            self.operator = lexa
            self.type = 'operator'
            # Arguments are parsed as evaluable blocks
            self.arguments = [parse(ilexa)  if type(ilexa) is list 
                                                else block(ilexa) 
                              for ilexa in lexon]

    # Evaluates the value of the block, given a dictionary of parent identifiers
    def eval(self, parent_eval_data={}):

        # Evaluation for all literal types
        if 'literal' in self.type:
            return self.value   # The value of the literal is returned

        # Evaluation of let block
        elif self.type == 'let-block':
            # Arguments are evaluated and then evaluation 
            # of block is delegated with new data
            return self.evaluable.eval(
                {**parent_eval_data,                                 # Parent data
                **{iden: self.variables[iden].eval(parent_eval_data) # Evaluation of new args
                    for iden in self.variables}})

        # Evaluation of if block
        elif self.type == 'if-block':
            if self.condition.eval(parent_eval_data): # The condition is evaluated
                return self.iftrue.eval(parent_eval_data)   # If asserted
            else:
                return self.iffalse.eval(parent_eval_data)  # If not

        #Evaluation of cond block
        elif self.type == 'cond-block':
            for condition, evaluable in zip(self.conditions, self.evaluables):
                if ((type(condition) is bool and condition) or condition.eval(parent_eval_data)):
                    return evaluable.eval(parent_eval_data)
        
        # Evaluation of an operator call
        elif self.type == 'operator':
            if self.operator in operators:          # Is a predefined operator
                return operators[self.operator](    # Correct operator is called
                        [arg.eval(parent_eval_data) for arg in self.arguments]) # With evaluated arguments
            
            elif self.operator in custom_operators: # Is a user-defined operator
                evaluated_arguments = {iden: arg.eval(parent_eval_data) # Arguments are prepared
                                        for (iden, arg) in zip(         # Reading argument names from custom operator
                                            custom_operators[self.operator].arguments,self.arguments)}
                evlt = custom_operators[self.operator].evaluable.eval(  # Body is evaluated with new args 
                        {**parent_eval_data, **evaluated_arguments})    # Overwriting old args
                return evlt
            else:
                print("Evaluation Error: Operator not defined")
                return
        
        # Evaluation of function definition
        elif self.type == 'function-def':
            custom_operators[self.name] = custom_operator(
                self.arguments, self.evaluable) # A new custom operator is added

        elif self.type == 'constant-def':
            constants[self.name] = self.value.eval(parent_eval_data)

        # Evaluation of identifier
        elif self.type == 'identifier':
            if self.name in parent_eval_data:
                return parent_eval_data[self.name] # The value is retrieved from evaluation data
            else:
                print("Evaluation Error: Identifier missing")
                return 
        
        # In any other case, return a non-value
        else:
            return None

# Parses ready-to-evaluate execution tree from lexered tree
def parse(tree):
    # because lexered tree is in form [lexa <lexon1> <lexon2> ...]
    blk = block(tree[0], tree[1:])
    return blk
